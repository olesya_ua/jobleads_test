<?php

namespace TaxApp\Repository;

use TaxApp\Entity\StateEntity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method StateEntity|null find($id, $lockMode = null, $lockVersion = null)
 * @method StateEntity|null findOneBy(array $criteria, array $orderBy = null)
 * @method StateEntity[]    findAll()
 * @method StateEntity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StateEntity::class);
    }
}
