<?php

namespace TaxApp\Domain;

use TaxApp\Entity\StateEntity;

class State
{
    /** @var string */
    private $name;

    /** @var County[]|array */
    private $counties;

    /**
     * State constructor.
     * @param string $name
     * @param County[] $counties
     */
    public function __construct(string $name, array $counties)
    {
        $this->name = $name;
        $this->counties = $counties;
    }

    public static function fromDoctrineEntity(StateEntity $stateEntity): State
    {
        $counties = [];

        foreach ($stateEntity->getCounties() as $countyEntity) {
            $counties[] = County::fromDoctrineEntity($countyEntity);
        }

        return new static($stateEntity->getName(), $counties);
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getCountiesCount(): int
    {
        return count($this->counties);
    }

    public function getCounties(): array
    {
        return $this->counties;
    }

    public function getAverageAmountOfTaxes(): float
    {
        return $this->getSumOfTaxes() / $this->getCountiesCount();
    }

    public function getSumOfTaxes(): float
    {
        return array_reduce(
            $this->counties,
            function ($sum, County $element) {
                return $sum + $element->getAmountOfTaxes();
            },
            0
        );
    }

    public function getAverageTaxRate(): float
    {
        $taxRateSum = array_reduce(
            $this->counties,
            function ($sum, County $element) {
                return $sum + $element->getTaxRate();
            },
            0
        );

        return $taxRateSum / $this->getCountiesCount();
    }
}
