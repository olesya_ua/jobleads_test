<?php

namespace TaxApp\Domain;

class Country
{
    /** @var State[]|array */
    private $states;

    /**
     * @param State[] $states
     */
    public function __construct(array $states)
    {
        $this->states = $states;
    }

    /**
     * @return State[]|array
     */
    public function getStates(): array
    {
        return $this->states;
    }
}
