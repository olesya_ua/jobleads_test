<?php

namespace TaxApp\Domain;

use TaxApp\Entity\CountyEntity;

class County
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var float
     */
    private $taxRate;

    /**
     * @var float
     */
    private $amountOfTaxes;

    public function __construct(string $name, float $taxRate, float $amountOfTaxes)
    {
        $this->name = $name;
        $this->taxRate = $taxRate;
        $this->amountOfTaxes = $amountOfTaxes;
    }

    public static function fromDoctrineEntity(CountyEntity $countyEntity): County
    {
        return new static(
            $countyEntity->getName(),
            $countyEntity->getTaxRate(),
            $countyEntity->getAmountOfTaxes()
        );
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getTaxRate(): float
    {
        return $this->taxRate;
    }

    public function getAmountOfTaxes(): float
    {
        return $this->amountOfTaxes;
    }
}
