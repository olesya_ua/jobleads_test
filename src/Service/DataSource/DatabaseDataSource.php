<?php

namespace TaxApp\Service\DataSource;

use TaxApp\Domain\Country;
use TaxApp\Domain\State;
use TaxApp\Repository\StateRepository;

class DatabaseDataSource implements DataSourceInterface
{
    /**
     * @var StateRepository
     */
    private $stateRepository;

    public function __construct(StateRepository $stateRepository)
    {
        $this->stateRepository = $stateRepository;
    }

    public function getCountry(): Country
    {
        $states = [];
        $stateEntities = $this->stateRepository->findAll();

        foreach ($stateEntities as $stateEntity) {
            $states[] = State::fromDoctrineEntity($stateEntity);
        }

        return new Country($states);
    }
}
