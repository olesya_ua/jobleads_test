<?php

namespace TaxApp\Service\DataSource;

use TaxApp\Domain\State;
use TaxApp\Domain\County;
use TaxApp\Domain\Country;

class XMLDataSource implements DataSourceInterface
{
    /**
     * @var \SimpleXMLElement
     */
    private $xmlData;

    public function __construct(string $filePath)
    {
        $this->xmlData = simplexml_load_file($filePath);
    }

    public function getCountry() : Country
    {
        $states = [];

        foreach ($this->xmlData->state as $rawState) {
            $counties = [];

            foreach ($rawState->county as $rawCounty) {
                $counties[] = new County(
                    $rawCounty['name'],
                    (float) $rawCounty->tax_rate,
                    (float) $rawCounty->amount_of_taxes
                );
            }

            $states[] = new State($rawState['name'], $counties);
        }

        return new Country($states);
    }
}
