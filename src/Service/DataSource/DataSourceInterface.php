<?php

namespace TaxApp\Service\DataSource;

use TaxApp\Domain\Country;

interface DataSourceInterface
{
    public function getCountry(): Country;
}
