<?php

namespace TaxApp\Service;

use TaxApp\Service\DataSource\DataSourceInterface;

class TaxCalculator
{
    /** @var DataSourceInterface */
    private $dataSource;

    public function __construct(DataSourceInterface $dataSource)
    {
        $this->dataSource = $dataSource;
    }

    public function getOverallAmountOfTaxesPerState(): array
    {
        $amountOfTaxesPerState = [];

        foreach ($this->getStates() as $state) {
            $amountOfTaxesPerState[$state->getName()] = $state->getSumOfTaxes();
        }

        return $amountOfTaxesPerState;
    }

    public function getAverageAmountOfTaxesPerState(): array
    {
        $amountOfTaxesPerState = [];

        foreach ($this->getStates() as $state) {
            $amountOfTaxesPerState[$state->getName()] = $state->getAverageAmountOfTaxes();
        }

        return $amountOfTaxesPerState;
    }

    public function getAverageCountyTaxRatePerState(): array
    {
        $averageTaxRatePerState = [];

        foreach ($this->getStates() as $state) {
            $averageTaxRatePerState[$state->getName()] = $state->getAverageTaxRate();
        }

        return $averageTaxRatePerState;
    }

    public function getAverageTaxRateOfTheCountry(): float
    {
        return array_sum($this->getAverageCountyTaxRatePerState()) / count($this->getStates());
    }

    public function getOverallTaxesOfTheCountry(): float
    {
        return array_sum($this->getOverallAmountOfTaxesPerState());
    }

    private function getStates(): array
    {
        return $this->dataSource->getCountry()->getStates();
    }
}
