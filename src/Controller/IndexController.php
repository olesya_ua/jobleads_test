<?php

namespace TaxApp\Controller;

use TaxApp\Service\TaxCalculator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    /**
     * @param TaxCalculator $taxCalculator
     *
     * @return Response
     *
     * @Route("/", name="index")
     */
    public function indexAction(TaxCalculator $taxCalculator)
    {
        return $this->render('index.html.twig', [
            'overallAmountOfTaxesPerState' => $taxCalculator->getOverallAmountOfTaxesPerState(),
            'averageAmountOfTaxesPerState' => $taxCalculator->getAverageAmountOfTaxesPerState(),
            'averageCountyTaxRatePerState' => $taxCalculator->getAverageCountyTaxRatePerState(),
            'averageTaxRateOfTheCountry' => $taxCalculator->getAverageTaxRateOfTheCountry(),
            'overallTaxesOfTheCountry' => $taxCalculator->getOverallTaxesOfTheCountry(),
        ]);
    }
}
