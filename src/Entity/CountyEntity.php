<?php

namespace TaxApp\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="county")
 */
class CountyEntity
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $stateId;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    private $taxRate;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    private $amountOfTaxes;

    /**
     * @var StateEntity
     * @ORM\ManyToOne(targetEntity="TaxApp\Entity\StateEntity", inversedBy="counties", cascade={"persist"})
     */
    private $state;

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getStateId(): int
    {
        return $this->stateId;
    }

    public function getTaxRate(): float
    {
        return $this->taxRate;
    }

    public function getAmountOfTaxes(): float
    {
        return $this->amountOfTaxes;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function setStateId(int $stateId): void
    {
        $this->stateId = $stateId;
    }

    public function setTaxRate(float $taxRate): void
    {
        $this->taxRate = $taxRate;
    }

    public function setAmountOfTaxes(float $amountOfTaxes): void
    {
        $this->amountOfTaxes = $amountOfTaxes;
    }

    public function getState(): StateEntity
    {
        return $this->state;
    }
}
