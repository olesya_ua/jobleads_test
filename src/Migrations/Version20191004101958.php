<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191004101958 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("
            INSERT INTO `state` (`id`, `name`)
            VALUES
                (1, 'East'),
                (2, 'West'),
                (3, 'South'),
                (4, 'North'),
                (5, 'Center');
        ");
        $this->addSql("
            INSERT INTO `county` (`id`, `state_id`, `name`, `tax_rate`, `amount_of_taxes`)
            VALUES
                (1, 1, 'Kharkiv', 16.8, 220.55),
                (2, 2, 'Lviv', 18.7, 350.75),
                (3, 3, 'Odesa', 25, 540.95),
                (4, 4, 'Chernihiv', 15.2, 180),
                (5, 5, 'Dnipro', 20.5, 200.5),
                (6, 5, 'Kyiv', 25, 560);
        ");
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('TRUNCATE TABLE county');
        $this->addSql('TRUNCATE TABLE state');
    }
}
