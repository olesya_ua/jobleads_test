#Statistics about the tax income

## How to set up this project
1. Install composer dependencies with CLI command `composer install`
2. Open `.env` and set `DATABASE_URL` environment variable value
3. Create database with CLI command `bin/console doctrine:database:create`
4. Create database scheme and populate it with the demodata by running `bin/console doctrine:migration:migrate`
5. Run a local web server with document root pointing to `public` directory

## Unit tests
Run `bin/phpunit`

## Datasources
1. Database
2. XML file `./data.xml`

This two types of datasources are implement the interface `./src/Service/DataSource/DataSourceInterface.php` and you can choose one of this at `./config/services.yaml`:
* `@TaxApp\Service\DataSource\DatabaseDataSource` for database
* `@TaxApp\Service\DataSource\XMLDataSource` for xml file

Initially the idea was to calculate everything within SQL queries, but when it came to the second data source, 
I decided to calculate everything in one place (PHP code).
But if you wanted to see how this can be done with SQL, here it is:

* Output the overall amount of taxes collected per state:
```sql
select state.name as stateName, sum(amount_of_taxes) as amoutOfTaxes
from county
join state 
  on county.state_id = state.id
group by state.name;
```
* Output the average amount of taxes collected per state
```sql
select state.name as stateName, avg(amount_of_taxes) as averageAmountOfTaxes
from county
join state 
  on county.state_id = state.id
group by state.name;
```
* Output the average county tax rate per state
```sql
select state.name as stateName, avg(county.tax_rate) as averageTaxRate
from county
join state 
  on county.state_id = state.id
group by state.name;
```
* Output the average tax rate of the country 
```sql
with stateAVG as (
  select avg(county.tax_rate) as countyTaxRate
  from county
  join state 
    on county.state_id = state.id
  group by state.name
 ) select avg(countyTaxRate) as avgTaxRatePerCountry
 from stateAVG;
```
* Output the collected overall taxes of the country
```sql
select sum(county.amount_of_taxes) as overallTaxes
from county
join state 
  on county.state_id = state.id;
```

## Refactoring task 

File `StringTool.php` is located in the root directory.


