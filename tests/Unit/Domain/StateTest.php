<?php

namespace TaxApp\Tests\Unit\Domain;

use PHPUnit\Framework\TestCase;
use TaxApp\Domain\County;
use TaxApp\Domain\State;

class StateTest extends TestCase
{
    public function testOk()
    {
        $county1 = new County('county1', 10, 4500);
        $county2 = new County('county2', 18.9, 9800.95);
        $county3 = new County('county3', 0, 0);
        $county4 = new County('county4', 14.7, 5456.75);

        $state = new State('state1', [$county1, $county2, $county3, $county4]);

        $this->assertEquals('state1', $state->getName());
        $this->assertEquals(4, $state->getCountiesCount());
        $this->assertEquals(10.9, $state->getAverageTaxRate());
        $this->assertEquals(19757.7, $state->getSumOfTaxes());
        $this->assertEquals(4939.425, $state->getAverageAmountOfTaxes());
    }
}
