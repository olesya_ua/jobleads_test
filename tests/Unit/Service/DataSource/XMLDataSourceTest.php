<?php

namespace TaxApp\Tests\Unit\Service\DataSource;

use PHPUnit\Framework\TestCase;
use TaxApp\Domain\Country;
use TaxApp\Domain\State;
use TaxApp\Service\DataSource\XMLDataSource;

class XMLDataSourceTest extends TestCase
{
    private $fileHandle;

    protected function setUp()
    {
        parent::setUp();

        $this->fileHandle = tmpfile();
    }

    protected function tearDown()
    {
        fclose($this->fileHandle);

        parent::tearDown();
    }

    public function testOk()
    {
        $dataSource = new XMLDataSource($this->generateXmlFile());

        $country = $dataSource->getCountry();
        $this->assertInstanceOf(Country::class, $country);

        $states = $country->getStates();
        $this->assertCount(2, $states);

        $firstState = reset($states);
        $this->assertInstanceOf(State::class, $firstState);
    }

    private function generateXmlFile(): string
    {
        $xmlString = '<?xml version="1.0" encoding="utf-8"?>
            <country>
                <state name="East">
                    <county name="Kharkiv">
                        <tax_rate>16.8</tax_rate>
                        <amount_of_taxes>220.55</amount_of_taxes>
                    </county>
                </state>
                <state name="Center">
                    <county name="2">
                        <tax_rate>20.5</tax_rate>
                        <amount_of_taxes>200.5</amount_of_taxes>
                    </county>
                    <county name="Kyiv">
                        <tax_rate>25</tax_rate>
                        <amount_of_taxes>560</amount_of_taxes>
                    </county>
                </state>
            </country>
        ';

        fwrite($this->fileHandle, $xmlString);
        $fileMeta = stream_get_meta_data($this->fileHandle);

        return $fileMeta['uri'];
    }
}
