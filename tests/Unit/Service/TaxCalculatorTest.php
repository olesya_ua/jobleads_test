<?php

namespace TaxApp\Tests\Unit\Service;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use TaxApp\Service\DataSource\DataSourceInterface;
use TaxApp\Service\TaxCalculator;
use TaxApp\Domain\County;
use TaxApp\Domain\State;
use TaxApp\Domain\Country;

class TaxCalculatorTest extends TestCase
{
    public function testOk()
    {
        /**
         * @var MockObject|DataSourceInterface $dataSource
         */
        $dataSource = $this->createMock(DataSourceInterface::class);

        $county1 = new County('county1', 10, 4500);
        $county2 = new County('county2', 18.9, 9800.95);
        $county3 = new County('county3', 0, 0);
        $county4 = new County('county4', 25.65, 15476.75);
        $county5 = new County('county5', 50.45, 35456.3);

        $county6 = new County('county6', 14.75, 6785.25);
        $county7 = new County('county7', 40.65, 25456);
        $county8 = new County('county8', 34.05, 12765.75);
        $county9 = new County('county9', 21.55, 12050);

        $state1 = new State('state1', [$county1, $county2, $county3, $county4, $county5]);
        $state2 = new State('state2', [$county6, $county7, $county8, $county9]);

        $country = new Country([$state1, $state2]);

        $dataSource->expects($this->any())->method('getCountry')->willReturn($country);

        $taxCalculator = new TaxCalculator($dataSource);

        $this->assertEquals(
            ['state1' => 65234, 'state2' => 57057],
            $taxCalculator->getOverallAmountOfTaxesPerState()
        );
        $this->assertEquals(
            ['state1' => 13046.8, 'state2' => 14264.25],
            $taxCalculator->getAverageAmountOfTaxesPerState()
        );
        $this->assertEquals(
            ['state1' => 21, 'state2' => 27.75],
            $taxCalculator->getAverageCountyTaxRatePerState()
        );
        $this->assertEquals(122291, $taxCalculator->getOverallTaxesOfTheCountry());
        $this->assertEquals(24.375, $taxCalculator->getAverageTaxRateOfTheCountry());
    }
}
