<?php

class StringTool
{
    public static function concat(string $a, string $b): string
    {
        return $a . $b;
    }

    public static function writeLn(string $a): void
    {
        echo $a . PHP_EOL;
    }

    public static function toUpperCase(string $string): string
    {
        return strtoupper($string);
    }

    public static function toLowerCase(string $input): string
    {
        return strtolower($input);
    }

    public static function hash(string $input): string
    {
        return static::md5($input);
    }

    public static function md5(string $input): string
    {
        return md5($input);
    }

    public static function sha512(string $input): string
    {
        return hash('sha512', $input);
    }
	
	public static function concatenate(string $a, string $b, string $c): string
    {
        return $a . $b . $c;
    }

    public static function replace(string $search, string $replace, string $needle): string
    {
        return str_replace($search, $replace, $needle);
    }

    public static function trim($input): string
    {
        return trim($input);
    }
}
